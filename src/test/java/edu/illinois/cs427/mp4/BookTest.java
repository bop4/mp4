package edu.illinois.cs427.mp4;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Test;

public class BookTest extends TestCase {

    public Book book1;
    public Book book2;
    public String title_1 = "The Title of Book1";
    public String title_2 = "The Title of Book2";
    public String author_1 = "Author1";
    public String author_2 = "Author1";

    @Test
    public void testFailure() throws Exception{
        try{
            String invalidString = "1234567890";
            book1 = new Book(invalidString);
            
        } catch (Exception e){
            fail("Shoud not be an error here");
        }
    }

    @Test
    public void testBookConstructor1() {
        // TODO implement this
        // test if the title and author correspond
        this.book1 = new Book(title_1, author_1);   
        this.book2 = new Book(title_2, author_2);
        assertEquals(title_1, book1.getTitle());
        assertEquals(title_2, book2.getTitle());
        assertEquals(author_1, book1.getAuthor());
    }

    @Test
    public void testBookConstructor2() throws Exception {
        this.book1 = new Book(title_1, author_1);
        ObjectMapper objectMapper = new ObjectMapper();
        String stringRepresentation = objectMapper.writeValueAsString(this.book1);
        book2 = new Book(stringRepresentation);
        assertEquals(title_1, book2.getTitle());
        assertEquals(author_1, book2.getAuthor());
    }

    @Test
    public void testGetStringRepresentation1() throws Exception{
        this.book1 = new Book(title_1, author_1); 
        String stringRepresentation = this.book1.getStringRepresentation();
        Book book = new Book(stringRepresentation);
        assertEquals(title_1, book.getTitle());
        assertEquals(author_1, book.getAuthor());
    }

    @Test
    public void testGetContainingCollections1() {
        //TODO implement this
        Collection linux = new Collection("linux");
        Collection os = new Collection("os");
        Collection cs = new Collection("cs");
        linux.setParentCollection(os);
        os.setParentCollection(cs);
        this.book1 = new Book(title_1, author_1);  
        assertEquals(linux.deleteElement(this.book1), false);
        assertEquals(linux.addElement(this.book1), true);
        List<Collection> containCollection = this.book1.getContainingCollections();
        assertEquals(containCollection.size(), 3);
        assertEquals(linux.addElement(this.book1), false);
        assertEquals(linux.deleteElement(this.book1), true);
        containCollection = this.book1.getContainingCollections();
        assertEquals(containCollection.size(), 0);
    }
}
