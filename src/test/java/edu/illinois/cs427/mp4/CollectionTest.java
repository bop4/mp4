package edu.illinois.cs427.mp4;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CollectionTest {
    Collection c = new Collection("marvel");
    Book b1 = new Book("spiderman", "stan lee");

    @Test
    public void testRestoreCollection1() throws Exception {
        //TODO implement this
        c.deleteElement(b1);
        String strRep = c.getStringRepresentation();
        Collection newC = c.restoreCollection(strRep);
        assertEquals(c.getName(), newC.getName());
        assertEquals(c.getElements().size(), newC.getElements().size());
        List<Element> e1 = c.getElements();
        List<Element> e2 = newC.getElements();
        for (int i = 0; i < e1.size(); i++) {
            if (e1.get(i) instanceof Book && e2.get(i) instanceof Book) {
                assertEquals(((Book) e1.get(i)).getTitle(), ((Book) e2.get(i)).getTitle());
                assertEquals(((Book) e1.get(i)).getAuthor(), ((Book) e2.get(i)).getAuthor());
            } else if (e1.get(i) instanceof Collection && e2.get(i) instanceof Collection) {
                assertEquals(((Collection) e1.get(i)).getName(), ((Collection) e2.get(i)).getName());
            } else {
                throw new Exception("Types are inconsistent");
            }
        }
    }

    @Test
    public void testGetStringRepresentation1() throws Exception {
        //TODO implement this
        String strRep = c.getStringRepresentation();
        ObjectMapper mapper = new ObjectMapper();
        Collection newC = mapper.readValue(strRep, Collection.class);
        assertEquals(c.getName(), newC.getName());
        List<Element> e1 = c.getElements();
        List<Element> e2 = newC.getElements();
        assertEquals(e1.size(), e2.size());
        for (int i = 0; i < e1.size(); i++) {
            if (e1.get(i) instanceof Book && e2.get(i) instanceof Book) {
                assertEquals(((Book) e1.get(i)).getTitle(), ((Book) e2.get(i)).getTitle());
                assertEquals(((Book) e1.get(i)).getAuthor(), ((Book) e2.get(i)).getAuthor());
            } else if (e1.get(i) instanceof Collection && e2.get(i) instanceof Collection) {
                assertEquals(((Collection) e1.get(i)).getName(), ((Collection) e2.get(i)).getName());
            } else {
                throw new Exception("Types are inconsistent");
            }
        }
    }

    @Test
    public void testAddElement1() {
        //TODO implement this
        assertEquals(c.addElement(b1), true);
        assertEquals(c.addElement(b1), false);
        List<Element> elements = c.getElements();
        assertEquals(c.getElements().size(), 1);
        Book book1 = (Book) (elements.get(0));
        assertEquals(book1.getTitle(), b1.getTitle());
    }

    @Test
    public void testDeleteElement1() {
        //TODO implement this
        assertEquals(c.deleteElement(b1), false);
        c.addElement(b1);
        assertEquals(c.deleteElement(b1), true);
        List<Element> elements = c.getElements();
        assertEquals(elements.size(), 0);
    }
    
    @Test
    public void testDeleteElement2() {
        Collection c1 = new Collection("c1");
        Collection c2 = new Collection("c2");
        Collection c3 = new Collection("c3");
        c1.addElement(c3);
        assertEquals(c1.deleteElement(c2), false);
        c1.addElement(c2);
        assertEquals(c1.deleteElement(c2), true);
        List<Element> elements = c1.getElements();
        assertEquals(elements.size(), 1);
    }
}
