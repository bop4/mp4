package edu.illinois.cs427.mp4;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class LibraryTest {
    Collection comics1 = new Collection("marvel");
    Collection comics2 = new Collection("dc");
    Book b1 = new Book("spiderman", "stan lee");
    Library lib = new Library();

    @Test
    public void testLibraryConstructorFromFile1() throws Exception {
        //TODO implement this
        lib.addCollection(comics1);
        lib.saveLibraryToFile("library.json");
        Library newLib = new Library("library.json");
        List<Collection> c1 = newLib.getCollections();
        List<Collection> c2 = lib.getCollections();
        assertEquals(c1.size(), c2.size());
        for (int i = 0; i < c1.size(); i++) {
            assertEquals(c1.get(i).getName(), c2.get(i).getName());
        }
    }

    @Test
    public void testSaveLibraryToFile1() throws Exception {
        //TODO implement this
        lib.addCollection(comics1);
        lib.addCollection(comics2);
        lib.saveLibraryToFile("library2.json");
        Library newLib = new Library("library2.json");
        List<Collection> c1 = newLib.getCollections();
        assertEquals(c1.size(), 2);
        assertEquals(c1.get(0).getName(), comics1.getName());
        assertEquals(c1.get(1).getName(), comics2.getName());
    }
}
