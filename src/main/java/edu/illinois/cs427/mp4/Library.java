package edu.illinois.cs427.mp4;

import java.io.File;
import java.util.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Container class for all the collections (that eventually contain books). Its
 * main responsibility is to save the collections to a file and restore them
 * from a file.
 */
public final class Library {
    private List<Collection> collections;

    /**
     * Builds a new, empty library.
     */ 
    Library() {
        this.collections = new ArrayList<Collection>();
    }

    /**
     * Builds a new library and restores its contents from the given file.
     *
     * @param fileName the file from where to restore the library.
     */
    @JsonIgnore
    public Library(String fileName) throws Exception {
        // TODO implement this
        this.collections = new ArrayList<Collection>();
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            Library library = objectMapper.readValue(new File(fileName), Library.class);
            for (int i=0; i<library.collections.size(); i++){
                this.addCollection(library.collections.get(i));
            }
        } catch (Exception e){
            // pass
        }

    }

    /**
     * Saved the contents of the library to the given file.
         *
     * @param fileName the file where to save the library
     */
    @JsonIgnore
    public void saveLibraryToFile(String fileName) throws Exception {
        // TODO implement this
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            objectMapper.writeValue(new File(fileName), this);
        } catch (Exception e){
            
        }
    }

    /**
     * Returns the collections contained in the library.
     *
     * @return library contained elements
     */
    public List<Collection> getCollections() {
        // TODO implement this
        return this.collections;
    }

    /**
     * Add collections to the library.
     *
     * @param collection to be added to the library
     */
    public void addCollection(Collection collection) {
        // TODO implement this
        this.collections.add(collection);
    }
   
}
