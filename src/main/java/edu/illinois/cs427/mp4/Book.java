package edu.illinois.cs427.mp4;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;



/**
 * This class contains the information needed to represent a book.
 */
public final class Book extends Element {
    private String title;
    private String author;


    Book(){}

    /**
     * Builds a book with the given title and author.
     *
     * @param title  the title of the book
     * @param author the author of the book
     */
    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    /**
     * Builds a book from the string representation,
     * which contains the title and author of the book.
     *
     * @param stringRepresentation the string representation
     */
    public Book(String stringRepresentation)  {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Book book = objectMapper.readValue(stringRepresentation, Book.class);

            this.title = book.getTitle();
            this.author = book.getAuthor();
        } catch (Exception e){

        }

    }

    /**
     * Returns the string representation of the given book.
     * The representation contains the title and author of the book.
     *
     * @return the string representation
     */
    @JsonIgnore
    public String getStringRepresentation() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String stringRep = objectMapper.writeValueAsString(this);
            return stringRep;
        } catch (Exception e){

        }
        // if anything wrong, return null
        return null;
 
    }

    /**
     * Returns all the collections that this book belongs to directly and indirectly.
     * Consider the following.
     * (1) Computer Science is a collection.
     * (2) Operating Systems is a collection under Computer Science.
     * (3) The Linux Kernel is a book under Operating System collection.
     * Then, getContainingCollections method for The Linux Kernel should return a list
     * of these two collections (Operating Systems, Computer Science), starting from
     * the direct collection to more indirect collections.
     *
     * @return the list of collections
     */
    @JsonIgnore
    public List<Collection> getContainingCollections() {
        List<Collection> listCollections = new ArrayList<>();
        Collection recursiveCollection = this.getParentCollection();
        // recursively find the collections
        while (true) {
            if (recursiveCollection == null){
                break;
            }
            listCollections.add(recursiveCollection);
            recursiveCollection = recursiveCollection.getParentCollection();
        }
        return listCollections;
    }

    /**
     * Returns the title of the book.
     *
     * @return the title
     */
    public String getTitle() {
        return this.title; 
    }
   
    /**
     * Returns the author of the book.
     *
     * @return the author
     */
    public String getAuthor() {
        return this.author; 
    }
}
