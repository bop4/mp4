package edu.illinois.cs427.mp4;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import com.fasterxml.jackson.annotation.*;


/**
 * An abstract class to represent an entity in the library.
 * The element can be either a book or a collection of books.
 * 
 */

 // debuged here for 2 hours. an infinite loop occured.
 // we refered to this website and get our problem solved:
 // https://github.com/FasterXML/jackson-databind/blob/master/src/test/java/com/fasterxml/jackson/databind/jsontype/TestPolymorphicWithDefaultImpl.java




@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({@JsonSubTypes.Type(value=Book.class, name="Book"),
               @JsonSubTypes.Type(value=Collection.class, name="Collection")})
@JsonIgnoreProperties(ignoreUnknown=true)

public abstract class Element {
    @JsonIgnore
    private Collection parentCollection;
    Element(){}
    
    /**
     * Get the parent collection of this element.
     *
     * @return parent collection
     */
    public Collection getParentCollection() {
        return parentCollection;
    }

    /**
     * Set the parent collection for this element.
     *
     * @param collection collection to be set as parent
     */
    public void setParentCollection(Collection collection) {
        parentCollection = collection;
    }
}
