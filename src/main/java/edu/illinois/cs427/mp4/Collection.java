package edu.illinois.cs427.mp4;

import java.util.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a collection of books or (sub)collections.
 */
public final class Collection extends Element {
    List<Element> elements;
    private String name;

    Collection(){}

    /**
     * Creates a new collection with the given name.
     *
     * @param name the name of the collection
     */
    public Collection(String name) {
        // TODO implement this
        this.elements = new ArrayList<Element>();
        this.name = name;
    }

    /**
     * Restores a collection from its given string representation.
     *
     * @param stringRepresentation the string representation
     */
    @JsonIgnore
    public static Collection restoreCollection(String stringRepresentation) {
        // TODO implement this
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(stringRepresentation, Collection.class);
        } catch (Exception e) {
            
        }
        return null;
    }

    /**
     * Returns the string representation of a collection. 
     * The string representation should have the name of this collection, 
     * and all elements (books/subcollections) contained in this collection.
     *
     * @return string representation of this collection
     */
    @JsonIgnore
    public String getStringRepresentation() {
        // TODO implement this
        ObjectMapper objectMapper = new ObjectMapper();
        String ret;
        try {
            ret = objectMapper.writeValueAsString(this);
            return ret;
        } catch (Exception e) {
            // TODO Auto-generated catch block
        }
        return null;
    }

    /**
     * Returns the name of the collection.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Adds an element to the collection.
     * If parentCollection of given element is not null,
     * do not actually add, but just return false.
     * (explanation: if given element is already a part of  
     * another collection, you should have deleted the element 
     * from that collection before adding to another collection)
     *
     * @param element the element to add
     * @return true on success, false on fail
     */
    public boolean addElement(Element element) {
        if (element.getParentCollection() == null){
            this.elements.add(element);
            element.setParentCollection(this);
            return true;
        }
        return false;
    }
    
    /**
     * Deletes an element from the collection.
     * Returns false if the collection does not have 
     * this element.
     * Hint: Do not forget to clear parentCollection
     * of given element 
     *
     * @param element the element to remove
     * @return true on success, false on fail
     */
    public boolean deleteElement(Element element) {
        // TODO implement this
        for (int i=0; i<this.elements.size(); i++){
            if (((this.elements.get(i) instanceof Book ) && (element instanceof Book)) ||
                ((this.elements.get(i) instanceof Collection ) && (element instanceof Collection))){
                if (this.elements.get(i) == element){
                    this.elements.remove(i);
                    element.setParentCollection(null);
                    return true;
                }
            }
        }
        
        return false;
    }
    
    /**
     * Returns the list of elements.
     * 
     * @return the list of elements
     */
    public List<Element> getElements() {
        // TODO implement this
        return this.elements;
    }
}
